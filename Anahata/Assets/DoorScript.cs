﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;



public class DoorScript : MonoBehaviour
{
    private string SceneName;
    public string DoorText;
    private int doorIndex;

    private TextMeshProUGUI doorTextBox;
    public bool disableTextOverride;

    private bool enabled;

    public bool DebugAnimation;

    [SerializeField]
    AudioClip openingSound;

    public enum Scene
    {
        Courage,
        Forgiveness,
        MainArea,
        Patience
    }

    public Scene SceneSelect;

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(gameObject);
        SetScenename();
        doorTextBox = gameObject.GetComponentInChildren<TextMeshProUGUI>();

        if (disableTextOverride == false)
            doorTextBox.text = DoorText;


        enabled = true;
        Invoke("DelayedRefrence", .1f);
    }


    void DelayedRefrence()
    {
        GameManager.instance.AddDoor(this.gameObject.GetComponent<DoorScript>());

    }

    // Update is called once per frame
    void Update()
    {
        if (DebugAnimation == true)
        {
            DebugAnimation = false;
            EnterDoor();
        }
    }

    public void ChangeDoorText(string netText)
    {
        doorTextBox.text = netText;

    }

    public void EnterDoor()
    {
        if (enabled == true)
        {
            ChangeDoorText("");
            gameObject.GetComponent<Animator>().SetBool("Open", true);
            GetComponent<AudioSource>().Play();
            StartCoroutine(EnterDelayed());
        }
    }

    public void DisableRoom()
    {
        enabled = false;

    }

    public int ReturnDoorIndex()
    {
        return doorIndex;
    }
    public string ReturnSceneName()
    {
        return SceneName;
    }

    private void SetScenename()
    {
        switch (SceneSelect)
        {
            case Scene.Courage:
                SceneName = "CourageOutside";
                doorIndex = 1;
                break;
            case Scene.Forgiveness:
                SceneName = "Forgiveness";
                doorIndex = 2;
                break;
            case Scene.MainArea:
                SceneName = "Blockout2";
                doorIndex = 3;
                break;
            case Scene.Patience:
                SceneName = "PatienceRoom";
                doorIndex = 4;
                break;
        }
    }

    IEnumerator EnterDelayed()
    {
        yield return new WaitForSeconds(7.5f);
        GameManager.instance.EnterDoor(doorIndex);

    }

    private void EnterDealyedTeam()
    {

        EnterDoor();
    }
}

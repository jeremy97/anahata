﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForgivnessEnemy : MonoBehaviour
{
    [SerializeField] private GameObject player;
    private Vector3 startPos;
    [SerializeField] private Transform prepAttackPosition;
    [SerializeField] private float checkIfHiTime;


    [SerializeField] private GameObject rightWeapon;
    [SerializeField] private GameObject leftWeapon;

    [SerializeField]
    AudioClip enemyOw;
    [SerializeField]
    AudioClip playerOw;


    bool attacked;

    bool movingToPlayer;
    bool movingBack;
    bool movingToAttakPosition;
    float lerpTime;
    [SerializeField] private float moveSpeed;
    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;

        if (checkIfHiTime == 0)
            checkIfHiTime = 10;
        if (moveSpeed == 0)
            moveSpeed = 1;

        movingToPlayer = false;
        movingBack = false;
        movingToAttakPosition = false;

        lerpTime = 0;
        StartCoroutine(AtttackPlayer());
    }

    // Update is called once per frame
    void Update()
    {
        Attack();
    }


    IEnumerator AtttackPlayer()
    {
        attacked = false;
        movingToAttakPosition = true;

        yield return new WaitForSeconds(checkIfHiTime);

        if (attacked == false)
        {
            Debug.Log("You win");
            GameManager.instance.ExitRoom();
        }
        else
        {
            StartCoroutine(AtttackPlayer());
        }
    }

    private void Attack()
    {
        if (movingToAttakPosition == true)
        {
            if (transform.position != prepAttackPosition.transform.position)
            {
                transform.position = Vector3.Lerp(startPos, prepAttackPosition.transform.position, lerpTime);
                lerpTime += Time.deltaTime / moveSpeed * .75f;
            }
            else
            {
                movingToAttakPosition = false;
                movingToPlayer = true;
                lerpTime = 0;
            }
        }
        else if (movingToPlayer == true)
        {
            if (transform.position != player.transform.position)
            {
                transform.position = Vector3.Lerp(prepAttackPosition.transform.position, player.transform.position, lerpTime);
                lerpTime += Time.deltaTime / moveSpeed * 1.25f;
            }
            else
            {
                AudioSource.PlayClipAtPoint(playerOw, transform.position);
                movingToPlayer = false;
                movingBack = true;
                lerpTime = 0;
            }

        }

        else if (movingBack == true)
        {
            if (transform.position != startPos)
            {
                transform.position = Vector3.Lerp(player.transform.position, startPos, lerpTime);
                lerpTime += Time.deltaTime / moveSpeed;
            }
            else
            {
                movingToPlayer = false;
                movingBack = false;
                lerpTime = 0;
            }

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.tag);
        if (other.tag == "Weapon")
        {
            Debug.Log("You Hit them");
            AudioSource.PlayClipAtPoint(enemyOw, transform.position);
            attacked = true;
        }
    }

    //IEnumerator TakeDamage()
    //{

    //}


}

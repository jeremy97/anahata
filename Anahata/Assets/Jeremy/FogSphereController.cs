﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogSphereController : MonoBehaviour
{
    [SerializeField] private GameObject m_FogSpherePrefab;
    [SerializeField] private GameObject m_FogParticlePrefab;
    [SerializeField] private int m_AmountToSpawn;
    [SerializeField] private float m_Duration;
    [SerializeField] private float m_MinRotateSpeed = -3f;
    [SerializeField] private float m_MaxRotateSpeed = 3f;

    //Dictionary containing each instantiated fog sphere and its corresponding rotation speed
    private Dictionary<GameObject, float> fogSpheres;
    private GameObject fogParticle;
    private float durationTimer;
    private float maxAlpha;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        Setup();
    }

    void SpawnSpheres()
    {
        for (int i = 0; i < m_AmountToSpawn; i++)
        {
            float rotationSpeed = Random.Range(m_MinRotateSpeed, m_MaxRotateSpeed);
            if (rotationSpeed >= 0f)
            {
                rotationSpeed += 5f;
            }
            else
            {
                rotationSpeed -= 5f;
            }
            GameObject go = Instantiate(m_FogSpherePrefab, transform);
            go.transform.Rotate(new Vector3(1, 0, 0), 90);
            fogSpheres.Add(go, rotationSpeed);
        }
    }

    private void Update()
    {
        durationTimer += Time.deltaTime;
        //How far are we into the animation? Range 0f-1f
        float timeIntoAnimation = durationTimer / m_Duration;

        //tweak transparency of material based on time into animation
        float alphaModifier;
        if (timeIntoAnimation < 0.5f)
        {
            //first half of animation, get an increasing val in the range of 0f-1f
            alphaModifier = timeIntoAnimation * 2f;
        }
        else
        {
            //second half of animation, get a decreasing val in the range of 0f-1f
            alphaModifier = 1 - ((timeIntoAnimation - 0.5f) * 2f);
        }

        //go through each fog sphere
        foreach (KeyValuePair<GameObject, float> pair in fogSpheres)
        {
            //Rotate the fog sphere
            pair.Key.transform.Rotate(Vector3.up, pair.Value * Time.deltaTime);
            //adjust the alpha of the material on this fog sphere
            MeshRenderer mr = pair.Key.GetComponent<MeshRenderer>();
            Color c = mr.material.color;
            c.a = maxAlpha * alphaModifier;
            mr.material.color = c;
        }

        //Track this to the camera position and rotation
        transform.position = Camera.main.transform.position;
        transform.rotation = Camera.main.transform.rotation;
        transform.Rotate(new Vector3(1, 0, 0), 90);

        //if time's up, destroy all the fog spheres and this object
        if (durationTimer >= m_Duration)
        {
            foreach (KeyValuePair<GameObject, float> pair in fogSpheres)
            {
                Destroy(pair.Key);
            }
            fogSpheres.Clear();
            Destroy(fogParticle);

            //Setup();
            Destroy(gameObject);
        }
    }

    void Setup()
    {
        fogSpheres = new Dictionary<GameObject, float>();
        maxAlpha = m_FogSpherePrefab.GetComponent<MeshRenderer>().sharedMaterial.color.a;
        durationTimer = 0f;

        //instantiate the fog spheres
        //StartCoroutine(SpawnSpheresDelayed());
        SpawnSpheres();
        fogParticle = Instantiate(m_FogParticlePrefab, transform);
    }
}
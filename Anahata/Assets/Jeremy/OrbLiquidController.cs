﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbLiquidController : MonoBehaviour
{
    public static OrbLiquidController instance;
    [SerializeField] private float m_MinFillLevel;
    [SerializeField] private float m_MaxFillLevel;
    public float m_FillLevel;
    private MeshRenderer mr;

    private void Start()
    {
       
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(transform.parent.gameObject);
        }
        else
            Destroy(gameObject);
        mr = GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        float input = (m_MaxFillLevel - m_MinFillLevel) * m_FillLevel + m_MinFillLevel;
        mr.sharedMaterial.SetFloat("_input", input);
        transform.rotation = Quaternion.identity;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GalaxySpin : MonoBehaviour
{
    [SerializeField] private float m_SpinRate = 5f;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.forward, m_SpinRate * Time.deltaTime);
    }
}

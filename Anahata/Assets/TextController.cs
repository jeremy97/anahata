﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextController : MonoBehaviour
{
    [SerializeField] private float m_AnimationLength = 6f;

    void Awake()
    {
        Invoke("Stop", m_AnimationLength);
    }

    void Stop()
    {
        Destroy(gameObject);
    }
}

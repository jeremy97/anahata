﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    public AudioSource[] tracks;
    public AudioClip[] loops;
    private List<float> maxVolumes = new List<float>();

    [Range(0,1)]
    public float volume = 0.5f;

    private void Awake()
    {
        for (int i = 0; i < tracks.Length; i++)
        {
            maxVolumes.Add(tracks[i].volume);
        }
    }

    private void Update()
    {
        for (int i = 0; i < tracks.Length; i++)
        {
            if (!tracks[i].isPlaying)
            {
                tracks[i].clip = loops[i];
                tracks[i].loop = true;
                tracks[i].Play();
            }
        }
    }

    private void SetVolume()
    {
        for (int i = 0; i < tracks.Length; i++)
        {
            tracks[i].volume = Mathf.Lerp(0, maxVolumes[i], volume);
        }
    }

    private void OnValidate()
    {
        if(maxVolumes.Count < tracks.Length)
        {
            maxVolumes.Clear();
            for (int i = 0; i < tracks.Length; i++)
            {
                maxVolumes.Add(tracks[i].volume);
            }
        }
        SetVolume();
    }

    // Gradually change the volume of the music player
    IEnumerator FadeTo(float aTime, float aVolume)
    {
        float vol = volume;

        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
        {
            volume = Mathf.Lerp(vol, aVolume, t);
            yield return null;
        }
    }
}

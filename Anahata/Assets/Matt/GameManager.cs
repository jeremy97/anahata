﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public GameObject TextPrefab;

    private Dictionary<int, DoorScript> doors = new Dictionary<int, DoorScript>();
    private int currentDoor;

    public int powerLevel = 1;

    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
        {
            instance = this;
            SceneManager.sceneLoaded += Upgrade;
        }
        else
        {
            Destroy(this.gameObject);

        }

        DontDestroyOnLoad(this.gameObject);
    }

    public void AddDoor(DoorScript newDoor)
    {

        if (doors.ContainsKey(newDoor.ReturnDoorIndex()))
        {
            Destroy(newDoor.gameObject);
        }
        else
        {
            doors.Add(newDoor.ReturnDoorIndex(), newDoor);

        }
    }

    public void EnterDoor(int doorIndex)
    {
        currentDoor = doorIndex;
        SceneManager.LoadScene(doors[doorIndex].ReturnSceneName());
    }

    public void ExitRoom()
    {
        Debug.Log("Exit");
        SceneManager.LoadScene("Blockout2");      
        doors[currentDoor].DisableRoom();
        powerLevel++;
       

        
    }

    public void Upgrade(Scene aScene, LoadSceneMode aMode)
    {
        if(SceneManager.GetSceneByBuildIndex(3) == aScene)
        {
            StartCoroutine(LerpLiquid(powerLevel));
            HandScript[] hands = FindObjectsOfType<HandScript>();
            for (int i = 0; i < hands.Length; i++)
            {
                hands[i].GetComponent<LineRenderer>().widthMultiplier = powerLevel;
                if (powerLevel == 4)
                {
                    //Change color
                    hands[i].GetComponent<LineRenderer>().material.color = Color.black;

                }
            }
        }

        Vector3 dir = Camera.main.transform.forward;
        dir.y = 0f;
        dir.Normalize();
        Vector3 pos = Camera.main.transform.position + dir * 2.5f;

        //spawn text
        switch (aScene.buildIndex)
        {
            case 1:
                GameObject go1 = Instantiate(TextPrefab, pos, Quaternion.identity, Camera.main.transform);
                SetFadeText(go1, "P A T I E N C E");
                break;
            case 2:
                GameObject go2 = Instantiate(TextPrefab, pos, Quaternion.identity, Camera.main.transform);
                SetFadeText(go2, "F O R G I V E N E S S");
                break;
            case 4:
                GameObject go3 = Instantiate(TextPrefab, pos, Quaternion.identity, Camera.main.transform);
                go3.transform.Rotate(Vector3.up, 90f);
                SetFadeText(go3, "C O U R A G E");
                break;
        }
    }

    public IEnumerator LerpLiquid(int currentLevel)
    {
        yield return new WaitForSeconds(2f);
        float finalFillAmount = 0.25f * currentLevel;
        while (OrbLiquidController.instance.m_FillLevel < finalFillAmount)
        {
            OrbLiquidController.instance.m_FillLevel += Time.deltaTime / 2;
            yield return new WaitForEndOfFrame();
        }
        if (finalFillAmount == 1f)
        {
            //do win stuff
        }
    }

    void SetFadeText(GameObject go, string text)
    {
        go.GetComponentInChildren<TMPro.TextMeshProUGUI>().SetText(text);
    }
}

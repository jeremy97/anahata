﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteInEditMode]
public class EnvironmentColorLerp : MonoBehaviour
{
    [Range(0,1)]
    public float enlightenment = 0.0f;
    public MeshRenderer[] renderers;    // Materials you want to lerp

    public Color darkFog;
    public Color lightFog;

    public Color darkTexture;
    public Color lightTexture;


    public void LerpColors()
    {
        /* This is breaking everything and I don't know why
        RenderSettings.fogColor = Color.Lerp(darkFog, lightFog, enlightenment);
        RenderSettings.fogDensity = Mathf.Lerp(0.0001f, 0.0008f, enlightenment);
        */

        //for (int i = 0; i < renderers.Length; i++)
        //{
        //    renderers[i].sharedMaterial.color = Color.Lerp(darkTexture, lightTexture, enlightenment);
        //}
    }

    private void OnValidate()
    {
        LerpColors();
    }
}

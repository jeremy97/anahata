﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectZoneScript : MonoBehaviour
{
    public GameObject FogSphere;
    private RaycastHit hit;
    private Vector3 gravity = new Vector3(0, -9.81f, 0);
    private Vector3 velocity = Vector3.zero;

    bool falling;
    // Start is called before the first frame update
    void Start()
    {
        falling = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (falling == false)
        {
            falling = CheckUnderneath();
        }
        else
        {
            velocity += gravity * Time.deltaTime;
            transform.parent.position += velocity * Time.deltaTime;
        }
    }


    private bool CheckUnderneath()
    {
        Debug.DrawRay(transform.position, Vector3.down * 2, Color.red);


        if (Physics.Raycast(transform.position, Vector3.down, out hit, 4f))
        {
            if (hit.transform.gameObject.tag == "CollisionZone")
            {
                Debug.Log("Collision");

                StartCoroutine(ExitLevel());
                return true;
            }
        }

        return false;
    }


    IEnumerator ExitLevel()
    {
        yield return new WaitForSeconds(1.0f);

        Instantiate(FogSphere, transform);

        yield return new WaitForSeconds(3.0f);

        GameManager.instance.ExitRoom();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PatienceManager : MonoBehaviour
{

    public static PatienceManager instance;
    [SerializeField]
    private TextMeshProUGUI text;
    private float textAlpha;

    private void Start()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
        CheckHands();
        textAlpha = text.color.a;
    }
    void Update()
    {

        if (still)
        {
            fogDensity += (Time.deltaTime /12);
            textAlpha += (Time.deltaTime / 100);
        }
        else
        {
            fogDensity -= (Time.deltaTime / 8);
            textAlpha -= (Time.deltaTime / 100);
        }
        fogDensity = Mathf.Clamp(fogDensity, 0.1f, 2.5f);
        textAlpha = Mathf.Clamp(textAlpha, 0.1f, 1);
        Debug.Log(fogDensity);
        Debug.Log(textAlpha + " ALPHA ");
        RenderSettings.fogDensity = fogDensity;
        text.color = new Color(text.color.r, text.color.g, text.color.b, textAlpha);
    }

    public void CheckHands()
    {
        StartCoroutine(CheckHandsHelper());
    }

    [SerializeField, Range(0, 1)]
    private float fogDensity = 0;
    [SerializeField]
    private bool still = false;
    [SerializeField]
    private Transform rightHand;
    [SerializeField]
    private Transform leftHand;
    [SerializeField]
    private Transform headSet;

    private Vector3 rightHandPos;
    private Vector3 leftHandPos;
    private Vector3 headPos;
    private IEnumerator CheckHandsHelper()
    {
        //while (stillCount < fogTicks)
        while (fogDensity < 2)
        {
            rightHandPos = rightHand.position;
            leftHandPos = leftHand.position;
            headPos = headSet.position;
            yield return new WaitForSeconds(.4f);
            if ((rightHandPos - rightHand.position).magnitude < .03 &&
                (leftHandPos - leftHand.position).magnitude < .03 &&
                (headPos - headSet.position).magnitude < .03)
            {
                Debug.Log(true);
                still = true;
            }
            else
            {
                Debug.Log(false);
                still = false;
            }

        }
        GameManager.instance.ExitRoom();
    }
}

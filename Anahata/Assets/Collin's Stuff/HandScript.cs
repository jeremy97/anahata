﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class HandScript : MonoBehaviour
{
    [SerializeField]
    private SteamVR_Input_Sources hand;
    bool pointing = false;
    private void Update()
    {
        if (SteamVR_Input._default.inActions.GrabPinch.GetStateDown(hand))
        {
            pointing = true;

        }

        if (pointing)
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward * 100, out hit))
            {
                GetComponent<LineRenderer>().enabled = true;
                GetComponent<LineRenderer>().SetPosition(0, transform.position);
                GetComponent<LineRenderer>().SetPosition(1, hit.point);
                if (SteamVR_Input._default.inActions.GrabPinch.GetStateUp(hand))
                {
                    GetComponent<LineRenderer>().enabled = false;
                    if (hit.transform.gameObject.GetComponent<DoorScript>())
                    {
                        hit.transform.gameObject.GetComponent<DoorScript>().EnterDoor();
                    }
                    else if (hit.transform.gameObject.GetComponentInChildren<OrbLiquidController>())
                    {
                        //StartCoroutine(LerpLiquid(GameManager.instance.powerLevel));
                    }

                    if (hit.transform.gameObject.tag == "Jump To Point")
                    {
                        transform.parent.position = new Vector3(hit.transform.position.x, transform.parent.position.y, hit.transform.position.z);
                        hit.transform.gameObject.GetComponent<BoxCollider>().enabled = false;
                    }
                    pointing = false;
                }
            }
            else
            {
                GetComponent<LineRenderer>().enabled = false;
            }
        }




    }
    public IEnumerator LerpLiquid(int currentLevel)
    {       
        float finalFillAmount = 0.25f * currentLevel;
        while (OrbLiquidController.instance.m_FillLevel < finalFillAmount)
        {
            OrbLiquidController.instance.m_FillLevel += Time.deltaTime/6;
            yield return new WaitForEndOfFrame();
        }
        if(finalFillAmount == 1f)
        {
            //do win stuff
        }
    }

}
